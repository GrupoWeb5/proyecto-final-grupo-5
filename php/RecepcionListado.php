<!DOCTYPE html>
<header>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../assets/css/estilos.css">
    <title> CRE - GRUPO 5</title>

    
    <!DOCTYPE html>
<header>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../assets/css/estilos.css">
    <link href = "../assets/js/validar.js">
    <title> CRE - GRUPO 5</title> <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    
        <ul id="menu">
                <li> <a href="../index.php"> Inicio</a></li>
                <li> <a href="../php/EquipoListado.php"> Equipos</a>
                    <ul>
                        <li> <a href="../php/EquipoAlta.php"> Alta</a></li>
                        <li> <a href="../php/EquipoBorrar.php"> Borrar</a></li>
                        <li> <a href="../php/EquipoModificar.php"> Modificar</a></li>
                    </ul>
                    </li>  <!-- con esta li cierro Equipos -->
                
                <li> <a href="../php/ClienteListado.php"> Clientes</a>
                    <ul>
                        <li> <a href="../php/ClienteAlta.php"> Alta</a></li>
                        <li> <a href="../php/ClienteBorrar.php"> Borrar</a></li>
                        <li> <a href="../php/ClienteModificar.php"> Modificar</a></li>
                    </ul>
                    </li>  <!-- con esta li cierro Clientes -->
                
                <li> <a href="../php/RecepcionListado.php"> Recepciones</a>
                    <ul>
                        <li> <a href="../php/RecepcionAlta.php"> Alta</a></li>
                        <li> <a href="../php/RecepcionBorrar.php"> Borrar</a></li>
                        <li> <a href="../php/RecepcionModificar.php"> Modificar</a></li>
                    </ul>
                    </li>  <!-- con esta li cierro Recepciones -->    
           
           
        </ul>  <!-- cierro id = "menu"-->
        
</header>

<body>
    <br>
        <table border = "1">
            <tr>
                <td>ID Recepción</td>
                <td>Detalle</td>
                <td>ID Cliente</td>
                <td>Nombre</td>
                <td>ID Equipo</td>
                <td>Descripción Equipo</td>
            </tr>

            <?php 

            include "conexion.php";

           /* $sql = ("SELECT clientes.id_cliente,
            clientes.nombre,
            recepciones.id_recepcion,
            recepciones.detalle,
            equipos.id_equipo,
            equipos.descripcion
            FROM */
            $sql = ("SELECT recepciones.id_recepcion,
                            recepciones.detalle,
                            clientes.id_cliente,
                            clientes.nombre,
                            equipos.id_equipo,
                            equipos.descripcion
            FROM clientes
                INNER JOIN recepciones
                ON clientes.id_cliente = recepciones.id_cliente
                INNER JOIN equipos
                ON equipos.id_equipo = recepciones.id_equipo"); 

            $resultado = mysqli_query($conexion, $sql);

            while ($mostrar = mysqli_fetch_array ($resultado)){
                ?>
            <tr>
                <td><?php echo $mostrar['id_recepcion']?></td>
                <td><?php echo $mostrar['detalle']?></td>
                <td><?php echo $mostrar['id_cliente']?></td>
                <td><?php echo $mostrar['nombre']?></td>
                <td><?php echo $mostrar['id_equipo']?></td>
                <td><?php echo $mostrar['descripcion']?></td>
            </tr>
         <?php
            }
            ?>
        </table>
</body>